FROM node:10

RUN npm install --global npm@latest node-gyp@latest

WORKDIR /app

COPY ./package*.json ./
RUN npm install

COPY . ./
RUN npm run build

# build final image
FROM node:10-slim

RUN apt update && apt install -y libpq5
RUN npm install --global wait-port@latest

RUN adduser ddk --uid 1100 --disabled-password
RUN mkdir -p /app/logs && chown ddk:ddk -R /app

WORKDIR /app

COPY --chown=ddk --from=0 /app/dist/ ./dist/
COPY --chown=ddk --from=0 /app/node_modules/ ./node_modules/
COPY --chown=ddk --from=0 /app/core/database/migrations/ ./core/database/migrations/
COPY --chown=ddk --from=0 /app/core/database/sql/ ./core/database/sql/
COPY --chown=ddk --from=0 /app/package.json /app/docker-entrypoint-prod.sh ./

RUN chmod +x docker-entrypoint-prod.sh

USER ddk
ENTRYPOINT /app/docker-entrypoint-prod.sh
