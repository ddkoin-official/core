import { BlockId } from 'shared/repository/block';
import config from 'shared/config';
import { IAssetVote, Transaction, TransactionModel } from 'shared/model/transaction';
import BlockStorageService from 'core/service/blockStorage';

class FailService {
    public getRightLastRoundBlockId = (id: BlockId): BlockId => {
        return config.CONSTANTS.FAIL.FAIL_TRUE_ROUND_BUILD_BLOCK_IDS.get(id) || id;
    }

    public isValidateBlockSlot = (slotNumber: number): boolean => {
        const isSlotNumberInBadRange = config.CONSTANTS.FAIL.SLOT_BROKEN_ROUND.some(([first, last]) => {
            return slotNumber >= first && slotNumber <= last;
        });

        return !isSlotNumberInBadRange;
    }

    public isVerifyBlock = (id: BlockId): boolean => {
        return !config.CONSTANTS.FAIL.BAD_BLOCK_IDS.has(id);
    }

    public isVerifyTransactionInThePast(trs: Transaction<any>): boolean {
        return trs.blockId && BlockStorageService.getHeaders(trs.blockId)
            && BlockStorageService.getHeaders(trs.blockId).height >
            config.CONSTANTS.START_FEATURE_BLOCK.FIX_TRANSACTION_IN_THE_PAST_BLOCK_HEIGHT;
    }

    public isFailedVoteReward(trs: TransactionModel<IAssetVote>): boolean {
        return config.CONSTANTS.FAIL.FAILED_VOTE_TRANSACTION_IDS.has(trs.id);
    }
}

export default new FailService();
