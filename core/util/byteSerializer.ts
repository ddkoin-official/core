import { Block } from 'shared/model/block';
import { serializeAssetTransaction } from 'shared/util/transaction';
import { createBufferObject, createBufferArray } from 'shared/util/byteSerializer';
import { BufferTypes } from 'shared/util/byteSerializer/types';
import { SchemaName } from 'shared/util/byteSerializer/config';

export const blockByteSerialize = (block: Block): Buffer => {
    const byteAssetsTransactions = block.transactions
        .map(trs => serializeAssetTransaction(trs));
    const byteBlock = createBufferObject({
        id: block.id,
        version: block.version,
        createdAt: block.createdAt,
        height: block.height,
        previousBlockId: block.previousBlockId,
        transactionCount: block.transactionCount,
        amount: block.amount,
        fee: block.fee,
        payloadHash: block.payloadHash,
        generatorPublicKey: block.generatorPublicKey,
        signature: block.signature,
        relay: block.relay,
        transactions: createBufferArray(
            byteAssetsTransactions,
            new BufferTypes.Object(SchemaName.TransactionBlock)
        ),
    }, SchemaName.Block);
    return byteBlock;
};
