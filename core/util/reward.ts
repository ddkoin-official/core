import DDK from 'ddk.registry';
import { AirdropType } from 'ddk.registry/dist/model/common/airdrop';
import { Stake } from 'ddk.registry/dist/model/common/transaction/stake';
import { Address, AirdropReward } from 'ddk.registry/dist/model/common/type';
import { calculateAirdropReward } from 'ddk.registry/dist/util/airdrop';
import { Account, AccountChangeAction } from 'shared/model/account';
import config from 'shared/config';
import { FactorAction, referredUsersFactory } from 'core/repository/referredUsers';
import { ResponseEntity } from 'shared/model/response';
import { IAssetStake, IAssetVote, Transaction, TransactionType } from 'shared/model/transaction';
import AccountRepo from 'core/repository/account';
import { isEqualMaps } from 'core/util/common';
import BlockRepository from 'core/repository/block';
import { isARPEnabled } from 'core/util/feature';
import { logger } from 'shared/util/logger';

export const getAirdropAddress = (type?: AirdropType): Address => {
    switch (type) {
        case AirdropType.AIRDROP:
            return config.CONSTANTS.AIRDROP.ADDRESS;
        case AirdropType.ARP:
            return BigInt(DDK.config.ARP.ADDRESS);
        default:
            break;
    }

    if (isARPEnabled()) {
        return getAirdropAddress(AirdropType.ARP);
    }
    return getAirdropAddress(AirdropType.AIRDROP);
};

export const getAirdropAccount = (type?: AirdropType): Account => {
    return AccountRepo.getByAddress(getAirdropAddress(type));
};

export const checkAirdrop = (
    transactionType: TransactionType,
    actual: AirdropReward,
    expected: AirdropReward,
): ResponseEntity => {
    if (!isEqualMaps(actual.sponsors, expected.sponsors)) {
        return new ResponseEntity<void>({
            errors: [
                `Verify failed: ${transactionType === TransactionType.STAKE ? 'stake' : 'vote'} ` +
                `airdrop reward is corrupted, ` +
                `expected: ${JSON.stringify([...expected.sponsors])}, ` +
                `actual: ${JSON.stringify([...actual.sponsors])}`
            ]
        });
    }

    return new ResponseEntity();
};

export const verifyStakeAirdrop = (
    trs: Transaction<IAssetVote | IAssetStake>,
    amount: number,
    sender: Account,
): ResponseEntity => {
    const lastBlock = BlockRepository.getLastBlock();
    const airdropAccount = getAirdropAccount();

    const expectedAirdrop = calculateAirdropReward(
        trs as any,
        amount,
        sender,
        lastBlock.height,
        airdropAccount.actualBalance,
        isARPEnabled(),
    );

    const response = checkAirdrop(trs.type, trs.asset.airdropReward, expectedAirdrop);

    if (lastBlock.height < config.CONSTANTS.ARP_REWARD_FIX_BLOCK_HEIGHT
        && response.errors && response.errors.length
        && trs.type == TransactionType.STAKE
        && trs.asset.airdropReward.sponsors.size < expectedAirdrop.sponsors.size) {
        for (const [key, val] of trs.asset.airdropReward.sponsors) {
            const testVal = expectedAirdrop.sponsors.get(key);
            // in cases of an undefined value, make sure the key
            // actually exists on the object so there are no false positives
            if (testVal !== val || (testVal === undefined && !expectedAirdrop.sponsors.has(key))) {
                return response;
            }
        }

        logger.debug(
            `[CORE][verifyStakeAirdrop]: found old incorrect rewards for stake: ` +
            `expected: ${JSON.stringify([...expectedAirdrop.sponsors])}, ` +
            `actual: ${JSON.stringify([...trs.asset.airdropReward.sponsors])}`
        );

        return new ResponseEntity();
    }

    return response;
};

export const verifyAirdrop = (
    trs: Transaction<IAssetVote | IAssetStake>,
    amount: number,
    sender: Account,
): ResponseEntity<void> => {
    if (trs.type === TransactionType.STAKE) {
        return verifyStakeAirdrop(trs, amount, sender);
    }

    const lastBlock = BlockRepository.getLastBlock();

    if (isARPEnabled()) {
        const arpAccount = AccountRepo.getByAddress(BigInt(DDK.config.ARP.ADDRESS));
        const arpAirdrop = calculateAirdropReward(
            trs as any,
            amount,
            sender,
            lastBlock.height,
            arpAccount.actualBalance,
            true,
        );

        const checkARPAirdrop = checkAirdrop(
            trs.type,
            (trs as Transaction<IAssetVote>).asset.arp.airdropReward, arpAirdrop
        );
        if (!checkARPAirdrop.success) {
            return checkARPAirdrop;
        }
    }

    const airdropAccount = AccountRepo.getByAddress(config.CONSTANTS.AIRDROP.ADDRESS);
    const commonAirdrop = calculateAirdropReward(
        trs as any,
        amount,
        sender,
        lastBlock.height,
        airdropAccount.actualBalance,
        false,
    );

    const checkCommonAirdrop = checkAirdrop(trs.type, trs.asset.airdropReward, commonAirdrop);
    if (!checkCommonAirdrop.success) {
        return checkCommonAirdrop;
    }

    return new ResponseEntity();
};

export function applyFrozeOrdersRewardAndUnstake(trs: Transaction<IAssetVote>, activeOrders: Array<Stake>): void {
    applyRewards(trs);
    applyUnstake(activeOrders, trs);
}

function applyRewards(trs: Transaction<IAssetVote>): void {
    AccountRepo.updateBalanceByAddress(trs.senderAddress, trs.asset.reward);
    AccountRepo.updateBalanceByAddress(config.CONSTANTS.TOTAL_SUPPLY.ADDRESS, -trs.asset.reward);

    if (trs.asset.arp && trs.asset.arp.reward) {
        AccountRepo.updateBalanceByAddress(trs.senderAddress, trs.asset.arp.reward);
        AccountRepo.updateBalanceByAddress(config.CONSTANTS.TOTAL_SUPPLY.ADDRESS, -trs.asset.arp.reward);
    }
}

function applyUnstake(orders: Array<Stake>, trs: Transaction<IAssetVote>): void {
    const readyToUnstakeOrders = orders.filter(o => o.voteCount === config.CONSTANTS.FROZE.UNSTAKE_VOTE_COUNT);
    readyToUnstakeOrders.map((order) => {
        order.isActive = false;
    });
    AccountRepo.updateBalanceByAddress(trs.senderAddress, trs.asset.unstake);
    referredUsersFactory
        .get()
        .updateStakeAmountFactor(trs.senderAddress, trs.asset.unstake, FactorAction.SUBTRACT);

    if (trs.asset.arp) {
        AccountRepo.updateBalanceByAddress(trs.senderAddress, trs.asset.arp.unstake);
        referredUsersFactory
            .get()
            .updateStakeAmountFactor(trs.senderAddress, trs.asset.arp.unstake, FactorAction.SUBTRACT);
    }
}

export function isSponsorsExist(trs: Transaction<IAssetStake | IAssetVote>): boolean {
    return trs.asset.airdropReward.sponsors.size !== 0 ||
        ((trs.asset as IAssetVote).arp && (trs.asset as IAssetVote).arp.airdropReward.sponsors.size !== 0);
}

export function sendAirdropReward(trs: Transaction<IAssetStake | IAssetVote>): void {
    let airdropAddress = config.CONSTANTS.AIRDROP.ADDRESS;
    if (trs.type === TransactionType.STAKE && isARPEnabled()) {
        airdropAddress = getAirdropAddress(AirdropType.ARP);
    }

    const transactionAirdropReward = trs.asset.airdropReward;

    for (const [sponsorAddress, rewardAmount] of transactionAirdropReward.sponsors) {
        if (rewardAmount === 0) {
            continue;
        }

        const recipient = AccountRepo.getByAddress(sponsorAddress);
        recipient.actualBalance += rewardAmount;
        recipient.addHistory(AccountChangeAction.AIRDROP_REWARD_RECEIVE, trs.id);

        AccountRepo.updateBalanceByAddress(airdropAddress, -rewardAmount);
    }

    if (trs.type === TransactionType.VOTE && (trs.asset as IAssetVote).arp) {
        const arpReferrers = (trs.asset as IAssetVote).arp.airdropReward.sponsors;
        for (const [sponsorAddress, rewardAmount] of arpReferrers) {
            if (rewardAmount === 0) {
                continue;
            }

            const recipient = AccountRepo.getByAddress(sponsorAddress);
            recipient.actualBalance += rewardAmount;

            AccountRepo.updateBalanceByAddress(getAirdropAddress(AirdropType.ARP), -rewardAmount);
        }
    }

    referredUsersFactory.get().updateRewardFactor(trs, FactorAction.ADD);
}

export function undoAirdropReward(trs: Transaction<IAssetVote | IAssetStake>): void {
    let airdropAddress = config.CONSTANTS.AIRDROP.ADDRESS;
    if (trs.type === TransactionType.STAKE && isARPEnabled()) {
        airdropAddress = getAirdropAddress(AirdropType.ARP);
    }

    const transactionAirdropReward = trs.asset.airdropReward;

    for (const [sponsorAddress, rewardAmount] of transactionAirdropReward.sponsors) {
        if (rewardAmount === 0) {
            continue;
        }
        const recipient = AccountRepo.getByAddress(sponsorAddress);
        recipient.actualBalance -= rewardAmount;
        recipient.addHistory(AccountChangeAction.AIRDROP_REWARD_RECEIVE_UNDO, trs.id);

        AccountRepo.updateBalanceByAddress(airdropAddress, rewardAmount);
    }

    referredUsersFactory.get().updateRewardFactor(trs, FactorAction.SUBTRACT);

    if (trs.type === TransactionType.VOTE && (trs.asset as IAssetVote).arp) {
        const arpReferrers = (trs.asset as IAssetVote).arp.airdropReward.sponsors;
        for (const [sponsorAddress, rewardAmount] of arpReferrers) {
            if (rewardAmount === 0) {
                continue;
            }

            const recipient = AccountRepo.getByAddress(sponsorAddress);
            recipient.actualBalance -= rewardAmount;

            AccountRepo.updateBalanceByAddress(getAirdropAddress(AirdropType.ARP), rewardAmount);
        }
    }
}

export function undoFrozeOrdersRewardAndUnstake(
    trs: Transaction<IAssetVote>,
    sender: Account,
    senderOnly: boolean
): void {
    const updatedOrders = sender.getAllStakes().filter((order: Stake) => {
        return order.nextVoteMilestone === trs.createdAt + config.CONSTANTS.FROZE.VOTE_MILESTONE;
    });
    undoUnstake(updatedOrders, trs, sender);
    undoRewards(trs, sender, senderOnly);
}

function undoUnstake(orders: Array<Stake>, trs: Transaction<IAssetVote>, sender: Account): void {
    const unstakedOrders = orders.filter(order => !order.isActive);
    unstakedOrders.map((order) => {
        order.isActive = true;
    });
    sender.actualBalance -= trs.asset.unstake;
    referredUsersFactory.get().updateStakeAmountFactor(sender.address, trs.asset.unstake, FactorAction.ADD);

    if (trs.asset.arp) {
        sender.actualBalance -= trs.asset.arp.unstake;
        referredUsersFactory
            .get()
            .updateStakeAmountFactor(sender.address, trs.asset.arp.unstake, FactorAction.ADD);
    }
}

function undoRewards(trs: Transaction<IAssetVote>, sender: Account, senderOnly: boolean): void {
    sender.actualBalance -= trs.asset.reward;

    if (trs.asset.arp && trs.asset.arp.reward) {
        sender.actualBalance -= trs.asset.arp.reward;
    }

    if (!senderOnly) {
        AccountRepo.updateBalanceByAddress(config.CONSTANTS.TOTAL_SUPPLY.ADDRESS, trs.asset.reward);

        if (trs.asset.arp && trs.asset.arp.reward) {
            AccountRepo.updateBalanceByAddress(config.CONSTANTS.TOTAL_SUPPLY.ADDRESS, trs.asset.arp.reward);
        }
    }
}
