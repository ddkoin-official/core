import { Stake } from 'ddk.registry/dist/model/common/transaction/stake';
import AirdropReferredUsersTree from 'core/repository/referredUsers/tree/AirdropReferredUsersTree';
import { Account } from 'shared/model/account';
import DDKRegistry from 'ddk.registry';
import {
    IAssetStake,
    IAssetVote,
    Transaction,
    TransactionType,
} from 'shared/model/transaction';
import { Address } from 'shared/model/types';

export default class ARPReferredUsersTree extends AirdropReferredUsersTree {

    protected getAccountReferrals(account: Account): Array<BigInt> {
        return account.arp.referrals;
    }

    protected getAccountActiveStakes(account: Account): Array<Stake> {
        return account.arp.stakes.filter(stake => stake.isActive);
    }

    protected getSponsors(trs: Transaction<IAssetStake | IAssetVote>): Map<Address, number> {
        if (trs.type === TransactionType.VOTE) {
            if ((trs.asset as IAssetVote).arp) {
                return (trs.asset as IAssetVote).arp.airdropReward.sponsors;
            }
            return new Map();
        }
        return trs.asset.airdropReward.sponsors;
    }

}
