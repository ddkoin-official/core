import { Fixture } from 'test/api/base/fixture';
import { expect } from 'chai';
import { API_ACTION_TYPES } from 'shared/driver/socket/codes';
import { socketRequest } from 'test/api/base';
import { getPreparedTransactionData } from 'test/api/base/util';
import { IAssetRegister, TransactionType, Transaction } from 'shared/model/transaction';

describe('Test CREATE_PREPARED_TRANSACTION REGISTER', () => {

    it('Positive', async () => {
        const REQUEST = {
            headers: Fixture.getBaseHeaders(),
            code: API_ACTION_TYPES.CREATE_PREPARED_TRANSACTION,
            body: {
                'createdAt': 0,
                'senderPublicKey': 'fc942085ce00a2cccb050d759b08af2022d24a5d83a5cf5e9c5b2f8f730ab3ab',
                'senderAddress': '8660093905399353926',
                'type': TransactionType.REGISTER,
                'salt': '68a9fec6a4467471c50161c53b4e937d',
                'asset': {
                    'referral': '4995063339468361088'
                },
                'fee': 0,
                'signature': 'a6836928d0b105d8d28355320605fd279c1bb8d2a8ffdb46f3fc1817b9265f8fb4a9c80e142c4f4f0505945730154c82915f65887879da7a0b520f57c9ecb805',
                'id': 'c302fed85860de34fa8e0e9c90d787415b65cc0d87c01b831fefe10af866f9c2'
            }
        };

        const SUCCESS = {
            'id': 'c302fed85860de34fa8e0e9c90d787415b65cc0d87c01b831fefe10af866f9c2',
            'type': TransactionType.REGISTER,
            'createdAt': 0,
            'senderPublicKey': 'fc942085ce00a2cccb050d759b08af2022d24a5d83a5cf5e9c5b2f8f730ab3ab',
            'senderAddress': '8660093905399353926',
            'signature': 'a6836928d0b105d8d28355320605fd279c1bb8d2a8ffdb46f3fc1817b9265f8fb4a9c80e142c4f4f0505945730154c82915f65887879da7a0b520f57c9ecb805',
            'fee': 0,
            'salt': '68a9fec6a4467471c50161c53b4e937d',
            'asset': {
                'referral': '4995063339468361088'
            }
        };

        const response = await socketRequest<any, Transaction<IAssetRegister>>(REQUEST);

        expect(response.body.success).to.equal(true);
        expect(getPreparedTransactionData<IAssetRegister>(response.body.data)).to.deep.equal(SUCCESS);
    });

    it('Negative', async () => {

        const REQUEST = {
            headers: Fixture.getBaseHeaders(),
            code: API_ACTION_TYPES.CREATE_PREPARED_TRANSACTION,
            body: {
                'type': TransactionType.REGISTER
            }
        };

        const FAILED = ['IS NOT VALID REQUEST:\'CREATE_PREPARED_TRANSACTION\'... Missing required property: asset'];

        const response = await socketRequest(REQUEST);

        expect(response.body.success).to.equal(false);
        expect(response.body.errors).to.deep.equal(FAILED);
    });
});
