import DDK, { WORKSPACE } from 'ddk.registry';
import AccountRepository from 'core/repository/account';
import TransactionService from 'core/service/transaction';
import { expect } from 'chai';
import { transactionCreator } from 'ddk.registry/dist/service/transaction';
import { TransactionType } from 'ddk.registry/dist/model/common/transaction/type';
import BlockRepository from 'core/repository/block';
import { AssetSend } from 'ddk.registry/dist/model/common/transaction/asset/send';
import config from 'shared/config';
import { Address } from 'shared/model/types';
import { Transaction } from 'ddk.registry/src/model/common/transaction/index';

describe('[Core][Transaction] Blacklist', () => {
    const blacklistedAddress = BigInt('4563480477309992764');
    const nonBlacklistedAddress = BigInt('7949459816978412754');
    let transaction: Transaction<AssetSend>;

    before(() => {
        config.CONSTANTS.BLACKLIST_ADDRESSES_HEIGHT = new Map<Address, number>([
            [blacklistedAddress, 3]
        ]);
        DDK.initialize(WORKSPACE.MAINNET, AccountRepository);

        const secret = 'program burger mammal sad digital wrestle turtle discover boring trend fun parrot';
        transaction = transactionCreator.create({
            data: {
                salt: '894cdfa99bc38ca098d38d305c811496',
                type: TransactionType.SEND,
                asset: new AssetSend({
                    recipientAddress: nonBlacklistedAddress,
                    amount: 1_0000_0000,
                }),
            },
            sender: undefined,
            secret,
        }).data as Transaction<AssetSend>;

        BlockRepository.add({height: 1} as any);
        BlockRepository.add({height: 2} as any);
    });

    describe('Verify by Blacklist before blocking height', () => {
        it('verify blacklisted sender and recipient', () => {
            transaction.senderAddress = blacklistedAddress;
            transaction.asset.recipientAddress = blacklistedAddress;
            const response = TransactionService.verifyByBlacklist(<any>transaction);
            expect(true).to.equal(response);
        });
    });

    describe('Verify by Blacklist after blocking height', () => {
        before(() => {
            BlockRepository.add({height: 3} as any);
        });

        beforeEach(() => {
            transaction.senderAddress = nonBlacklistedAddress;
            transaction.asset.recipientAddress = nonBlacklistedAddress;
        });

        it('verify not blacklisted sender and recipient', () => {
            const response = TransactionService.verifyByBlacklist(<any>transaction);
            expect(true).to.equal(response);
        });

        it('verify blacklisted sender', () => {
            transaction.senderAddress = blacklistedAddress;
            const response = TransactionService.verifyByBlacklist(<any>transaction);
            expect(false).to.equal(response);
        });

        it('verify blacklisted recipient', () => {
            transaction.asset.recipientAddress = blacklistedAddress;
            const response = TransactionService.verifyByBlacklist(<any>transaction);
            expect(false).to.equal(response);
        });
    });
});
