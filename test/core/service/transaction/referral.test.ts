import DDK, { WORKSPACE } from 'ddk.registry';
import { expect } from 'chai';
import { transactionCreator } from 'ddk.registry/dist/service/transaction';
import { TransactionType } from 'ddk.registry/dist/model/common/transaction/type';
import { AssetReferral } from 'ddk.registry/dist/model/common/transaction/asset/referral';
import TransactionService from 'core/service/transaction';
import AccountRepository from 'core/repository/account';
import BlockRepository from 'core/repository/block';

describe('[Core][Transaction] Referral', () => {
    let referrerAccount: any;
    let senderAccount: any;
    let referralTransaction: any;

    before(() => {
        const secret = 'truth there country plug seven world sniff vague bicycle cannon hair artefact';
        const transactionResponse = transactionCreator.create({
            data: {
                salt: '894cdfa99bc38ca098d38d305c811496',
                type: TransactionType.REFERRAL,
                asset: new AssetReferral({
                    referral: BigInt('4995063339468361088'),
                }),
            },
            sender: undefined,
            secret,
        });

        referralTransaction = transactionResponse.data as any;

        BlockRepository.add({ height: 1 } as any);
    });

    describe('Before ARP launch', () => {
        before(() => {
            DDK.initialize(WORKSPACE.MAINNET, AccountRepository);
        });

        beforeEach(() => {
            referrerAccount = AccountRepository.add({ address: referralTransaction.asset.referral });
            referrerAccount.arp.stakes.push({} as any);

            senderAccount = AccountRepository.add({
                address: referralTransaction.senderAddress,
                publicKey: referralTransaction.senderPublicKey,
            });
        });

        afterEach(() => {
            AccountRepository.delete(referrerAccount);
            AccountRepository.delete(senderAccount);
        });

        it('verify unconfirmed without referrer', () => {
            AccountRepository.delete(referrerAccount);

            const response = TransactionService.verifyUnconfirmed(referralTransaction, senderAccount);

            expect(true).to.equal(response.success);
        });

        it('verify unconfirmed', () => {
            const response = TransactionService.verifyUnconfirmed(referralTransaction, senderAccount);

            expect(true).to.equal(response.success);
        });

        it('apply unconfirmed', () => {
            TransactionService.applyUnconfirmed(referralTransaction, senderAccount);

            senderAccount = AccountRepository.getByAddress(senderAccount.address);

            expect(1).to.equal(senderAccount.referrals.length);
        });

        it('undo unconfirmed', () => {
            TransactionService.applyUnconfirmed(referralTransaction, senderAccount);
            TransactionService.undoUnconfirmed(referralTransaction, senderAccount);

            expect(0).to.equal(senderAccount.referrals.length);
        });
    });

    describe('After ARP launch', () => {
        before(() => {
            DDK.initialize(WORKSPACE.DEVELOPMENT, AccountRepository);
        });

        beforeEach(() => {
            referrerAccount = AccountRepository.add({ address: referralTransaction.asset.referral });
            referrerAccount.arp.stakes.push({} as any);

            senderAccount = AccountRepository.add({
                address: referralTransaction.senderAddress,
                publicKey: referralTransaction.senderPublicKey,
            });
        });

        afterEach(() => {
            AccountRepository.delete(referrerAccount);
            AccountRepository.delete(senderAccount);
        });

        it('verify unconfirmed without referrer', () => {
            AccountRepository.delete(referrerAccount);

            const response = TransactionService.verifyUnconfirmed(referralTransaction, senderAccount);

            expect(false).to.equal(response.success);
        });

        it('verify unconfirmed', () => {
            const response = TransactionService.verifyUnconfirmed(referralTransaction, senderAccount);

            expect(true).to.equal(response.success);
        });

        it('apply unconfirmed', () => {
            TransactionService.applyUnconfirmed(referralTransaction, senderAccount);

            senderAccount = AccountRepository.getByAddress(senderAccount.address);

            expect(1).to.equal(senderAccount.arp.referrals.length);
        });

        it('undo unconfirmed', () => {
            TransactionService.applyUnconfirmed(referralTransaction, senderAccount);
            TransactionService.undoUnconfirmed(referralTransaction, senderAccount);

            expect(0).to.equal(senderAccount.arp.referrals.length);
        });
    });
});
