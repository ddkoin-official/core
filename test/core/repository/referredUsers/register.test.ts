import DDK, { WORKSPACE } from 'ddk.registry';
import { expect } from 'chai';
import { transactionCreator } from 'ddk.registry/dist/service/transaction';
import { TransactionType } from 'ddk.registry/dist/model/common/transaction/type';
import { AssetReferral } from 'ddk.registry/dist/model/common/transaction/asset/referral';
import TransactionService from 'core/service/transaction';
import AccountRepository from 'core/repository/account';
import BlockRepository from 'core/repository/block';
import { referredUsersFactory } from 'core/repository/referredUsers/index';
import config from 'shared/config';

describe('[Core][ReferredUsers] Referral', () => {
    let referralTransaction: any;
    let referrerAccount: any;
    let senderAccount: any;

    before(() => {
        config.CORE.IS_REFERRED_USERS_ENABLED = true;
        DDK.initialize(WORKSPACE.MAINNET, AccountRepository);
    });

    before(() => {
        const secret = 'double local armor link toss hotel domain anxiety spot name polar drum';
        const transactionResponse = transactionCreator.create({
            data: {
                salt: '894cdfa99bc38ca098d38d305c811496',
                type: TransactionType.REFERRAL,
                asset: new AssetReferral({
                    referral: BigInt('4995063339468361088'),
                }),
            },
            sender: undefined,
            secret,
        });

        referralTransaction = transactionResponse.data as any;

        BlockRepository.add({ height: 1 } as any);
    });

    beforeEach(() => {
        referrerAccount = AccountRepository.add({ address: referralTransaction.asset.referral });

        senderAccount = AccountRepository.add({
            address: referralTransaction.senderAddress,
            publicKey: referralTransaction.senderPublicKey,
        });
    });

    afterEach(() => {
        AccountRepository.delete(referrerAccount);
        AccountRepository.delete(senderAccount);
    });

    it('apply unconfirmed', () => {
        TransactionService.applyUnconfirmed(referralTransaction, senderAccount);

        const referredUsers = referredUsersFactory.get().getUsers(referrerAccount, 1);

        expect(senderAccount.address).to.equal(referredUsers[0].address);
    });

    it('undo unconfirmed', () => {
        TransactionService.undoUnconfirmed(referralTransaction, senderAccount);

        const referredUsers = referredUsersFactory.get().getUsers(referrerAccount, 1);

        expect(referredUsers).to.deep.equal([]);
    });

});
