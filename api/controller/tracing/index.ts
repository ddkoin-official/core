import { RPC } from 'api/utils/decorators';
import { Message } from 'shared/model/message';
import { API_ACTION_TYPES } from 'shared/driver/socket/codes';
import { tracing } from 'api/middleware/tracing';
import SocketMiddleware from 'api/middleware/socket';
import { ResponseEntity } from 'shared/model/response';

class TracingController {
    constructor() {
        this.getMessages = this.getMessages.bind(this);
    }

    @RPC(API_ACTION_TYPES.GET_TRACING)
    public getMessages(message: Message<any>, socket: any): void {
        SocketMiddleware.emitToClient(
            message.headers.id,
            message.code,
            new ResponseEntity({ data: tracing.getMessages() }),
            socket
        );
    }
}

export default new TracingController();
