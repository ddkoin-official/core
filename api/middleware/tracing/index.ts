import { Message } from 'shared/model/message';
import { ResponseEntity } from 'shared/model/response';
import SocketIO from 'socket.io';
import { logger } from 'shared/util/logger';
import { API_ACTION_TYPES } from 'shared/driver/socket/codes';

type TracingMessage = {
    address: string;
    timeStart: Date;
    timeEnd?: Date;
    request: Message<any>;
};

export class Tracing {

    private messages: Map<string, Map<string, TracingMessage>>;

    private history: Map<string, Array<[string, string]>>;

    constructor() {
        this.messages = new Map();
        this.history = new Map();
    }

    private getTimeKey(date: Date): string {
        return `${date.getUTCFullYear()}-${date.getUTCMonth()}-${date.getUTCDate()}-${date.getUTCHours()}`;
    }

    addMessage(message: Message<ResponseEntity<any>>, socket: SocketIO.Socket) {
        const tracingMessage: TracingMessage = {
            address: socket.handshake.address,
            request: message,
            timeStart: new Date()
        };

        if (!this.messages.has(message.code)) {
            this.messages.set(message.code, new Map());
        }

        this.messages
            .get(message.code)
            .set(message.headers.id, tracingMessage);

        this.addHistory(tracingMessage);
    }

    private addHistory(tracingMessage: TracingMessage) {
        const timeKey = this.getTimeKey(tracingMessage.timeStart);
        if (!this.history.has(timeKey)) {
            this.history.set(timeKey, []);
        }

        this.history.get(timeKey).push([
            tracingMessage.request.code,
            tracingMessage.request.headers.id
        ]);

        if (this.history.size > 1) {
            const first = this.history.keys().next().value;
            for (const [code, id] of this.history.get(first)) {
                this.deleteMessage(code, id);
            }
            this.history.delete(first);
        }
    }

    deleteMessage(code: string, id: string) {
        const map = this.messages.get(code);
        if (map) {
            map.delete(id);
        }
    }

    finishMessage(message: Message<ResponseEntity<any>>) {
        const map = this.messages.get(message.code);
        if (map) {
            const item = map.get(message.headers.id);
            if (item) {
                item.timeEnd = new Date();

                if (item.request.code !== API_ACTION_TYPES.GET_TRACING) {
                    logger.trace(`[API][Tracing]: ${JSON.stringify(item)}`);
                }
            }
        }
    }

    getMessages() {
        const result = [];
        for (const [code, messageMap] of this.messages) {
            const messages = [];
            for (const [id, message] of messageMap) {
                messages.push([id, message]);
            }
            result.push([code, messages]);
        }
        return result;
    }
}

export const tracing = new Tracing();
