import { API_ACTION_TYPES } from 'shared/driver/socket/codes';
import { PAGINATION_SCHEME } from 'shared/validate/schema/common';
import { AirdropType } from 'ddk.registry/dist/model/common/airdrop';

export const ALLOWED_AIRDROP_TYPE = [
    AirdropType.AIRDROP,
    AirdropType.ARP,
];

export const SCHEMAS_REWARD = [
    {
        id: API_ACTION_TYPES.GET_REWARD_HISTORY,
        type: 'object',
        properties: {
            address: {
                type: 'string',
                format: 'address'
            },
            ...PAGINATION_SCHEME
        },
        required: ['address', 'limit', 'offset']
    },
    {
        id: API_ACTION_TYPES.GET_REFERRED_USERS_REWARDS,
        type: 'object',
        properties: {
            address: {
                type: 'string',
                format: 'address'
            },
            ...PAGINATION_SCHEME
        },
        required: ['address', 'limit', 'offset']
    },
    {
        id: API_ACTION_TYPES.GET_STAKE_REWARDS,
        type: 'object',
        properties: {
            senderPublicKey: {
                type: 'string',
                format: 'publicKey'
            },
            airdropTypes: {
                type: 'array',
                items: {
                    type: 'string',
                    enum: ALLOWED_AIRDROP_TYPE,
                }
            },
            ...PAGINATION_SCHEME
        },
        required: ['senderPublicKey', 'limit', 'offset']
    },
    {
        id: API_ACTION_TYPES.GET_AIRDROP_REWARDS,
        type: 'object',
        properties: {
            address: {
                type: 'string',
                format: 'address'
            },
            ...PAGINATION_SCHEME
        },
        required: ['address', 'limit', 'offset']
    }
];
