import { IAssetVote } from 'shared/model/transaction';
import { IAssetRepository } from 'shared/repository/transaction';
import { RawAsset } from 'shared/model/types';
import { Address } from 'ddk.registry/dist/model/common/type';

class TransactionVoteRepo implements IAssetRepository<IAssetVote> {
    serialize(asset: IAssetVote): RawAsset {
        const arp = asset.arp && {
            reward: asset.arp.reward,
            unstake: asset.arp.unstake,
            airdropReward: {
                sponsors: Array.from(asset.arp.airdropReward.sponsors)
                    .map(elem => [elem[0].toString(), elem[1]])
            },
        };

        return {
            votes: asset.votes,
            reward: asset.reward,
            unstake: asset.unstake,
            airdropReward: {
                sponsors: Array.from(asset.airdropReward.sponsors)
                    .map(elem => [elem[0].toString(), elem[1]])
            },
            arp,
        };
    }

    deserialize(rawAsset: RawAsset): IAssetVote {
        const arp = rawAsset.arp && {
            reward: rawAsset.arp.reward,
            unstake: rawAsset.arp.unstake,
            airdropReward: {
                sponsors: new Map<Address, number>(rawAsset.arp.airdropReward.sponsors
                    .map((elem: [string, number]) => [BigInt(elem[0]), elem[1]]))
            },
        };

        return {
            votes: rawAsset.votes,
            reward: rawAsset.reward,
            unstake: rawAsset.unstake,
            airdropReward: {
                sponsors: new Map(rawAsset.airdropReward.sponsors
                    .map((elem: [string, number]) => [BigInt(elem[0]), elem[1]]))
            },
            arp,
        };
    }
}

export default new TransactionVoteRepo();
