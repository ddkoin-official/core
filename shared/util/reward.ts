import { StakeReward } from 'shared/model/reward';
import { IAssetVote, Transaction } from 'shared/model/transaction';
import { AirdropType } from 'ddk.registry/dist/model/common/airdrop';

export const createStakeReward = (
    transaction: Transaction<IAssetVote>,
    airdropTypes: Array<AirdropType>,
): StakeReward => {
    let reward = 0;
    if (airdropTypes.includes(AirdropType.AIRDROP)) {
        reward += transaction.asset.reward;
    }
    if (transaction.asset.arp && airdropTypes.includes(AirdropType.ARP)) {
        reward += transaction.asset.arp.reward;
    }

    return new StakeReward({
        createdAt: transaction.createdAt,
        amount: reward,
    });
};
