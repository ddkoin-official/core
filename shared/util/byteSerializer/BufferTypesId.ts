export enum BufferTypesId {
    Int8,
    Uint8,
    Int16,
    Uint16,
    Int32,
    Uint32,
    Uint64,
    Double,
    Utf8,
    BufferArray,
    BufferObject,
    BufferSet,
    BufferMap,
    Bool,
    Number64
}
