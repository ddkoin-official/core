import { Int8 } from 'shared/util/byteSerializer/models/int8';
import { Uint8 } from 'shared/util/byteSerializer/models/uint8';
import { Int16 } from 'shared/util/byteSerializer/models/int16';
import { Uint16 } from 'shared/util/byteSerializer/models/uint16';
import { Int32 } from 'shared/util/byteSerializer/models/int32';
import { Uint32 } from 'shared/util/byteSerializer/models/uint32';
import { Uint64 } from 'shared/util/byteSerializer/models/uint64';
import { Double } from 'shared/util/byteSerializer/models/double';
import { Utf8 } from 'shared/util/byteSerializer/models/utf8';
import { BufferArray } from 'shared/util/byteSerializer/models/array';
import { BufferObject } from 'shared/util/byteSerializer/models/object';
import { DynamicBuffer } from 'shared/util/byteSerializer/models/dynamicBuffer';
import { BufferMap } from 'shared/util/byteSerializer/models/map';
import { BufferSet } from 'shared/util/byteSerializer/models/set';
import { Bool } from 'shared/util/byteSerializer/models/boolean';
import { Number64 } from 'shared/util/byteSerializer/models/number64';
import { BufferTypesId } from 'shared/util/byteSerializer/BufferTypesId';

export const BufferTypes = {
    Int8,
    Uint8,
    Int16,
    Uint16,
    Int32,
    Uint32,
    Uint64,
    Double,
    Utf8,
    Array: BufferArray,
    Object: BufferObject,
    Buffer: DynamicBuffer,
    Set: BufferSet,
    Map: BufferMap,
    Boolean: Bool,
    Number64,
};

export const mapPrimitiveCode = new Map();

mapPrimitiveCode.set(BufferTypesId.Int8, Int8);
mapPrimitiveCode.set(BufferTypesId.Uint8, Uint8);
mapPrimitiveCode.set(BufferTypesId.Int16, Int16);
mapPrimitiveCode.set(BufferTypesId.Uint16, Uint16);
mapPrimitiveCode.set(BufferTypesId.Int32, Int32);
mapPrimitiveCode.set(BufferTypesId.Uint32, Uint32);
mapPrimitiveCode.set(BufferTypesId.Uint64, Uint64);
mapPrimitiveCode.set(BufferTypesId.Double, Double);
mapPrimitiveCode.set(BufferTypesId.Utf8, Utf8);
mapPrimitiveCode.set(BufferTypesId.BufferArray, BufferArray);
mapPrimitiveCode.set(BufferTypesId.BufferObject, BufferObject);
mapPrimitiveCode.set(BufferTypesId.BufferSet, BufferSet);
mapPrimitiveCode.set(BufferTypesId.BufferMap, BufferMap);
mapPrimitiveCode.set(BufferTypesId.Bool, Bool);
mapPrimitiveCode.set(BufferTypesId.Number64, Number64);
